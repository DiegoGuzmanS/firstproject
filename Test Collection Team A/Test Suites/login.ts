<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>login</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-07-13T12:41:19</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>14fe0c97-6c9b-48dd-9bc3-fdafcc85045c</testSuiteGuid>
   <testCaseLink>
      <guid>361ca315-0190-4f7b-b246-9a18259dd10c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/firstCreate</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9b7161ec-c225-4270-b86e-454894d8ae7e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Negative Login to Testing SDMC</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a46b568d-adb1-4b48-9f5f-20c44d5399db</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login to SDMC</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c7555665-d0cf-4d41-936b-9b793b7a0879</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/NewPasswordsDontMatch</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>124086e0-35bc-4e78-96ef-d72f865b7a34</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/PasswordLessThan6Chars</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e7b25f5e-22df-4a9a-ba94-04d1004ae250</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/IncorrectCurrentPassword</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>adadd7d5-f31d-465a-818c-7cb190e59263</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ChangePasswordCORRECT</testCaseId>
   </testCaseLink>
</TestSuiteEntity>

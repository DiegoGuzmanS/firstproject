import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.exception.StepFailedException
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
////Navigate to SDMC page and attempt to log in
//WebUI.openBrowser('')
//WebUI.navigateToUrl('http://sdmc-katalon.azurewebsites.net/')
//WebUI.setText(findTestObject('Page_SDMC/input_input-username'), 'KATALON5566')
//WebUI.setText(findTestObject('Page_SDMC/input_input-password'), 'KATALON5566')
//WebUI.click(findTestObject('Page_SDMC/button_Sign In'))
//
////Make sure the user was redirected to change password page after login attempt
//WebUI.verifyElementPresent(findTestObject('Page_SDMC/changepass'), 5) 



//Input incorrect current password and attempt to login
// correct password is "Saaaam"
WebUI.setText(findTestObject('Page_SDMC/input_input-CurrentPassword'), 'Saaaamb')
WebUI.setText(findTestObject('Page_SDMC/input_input-NewPassword'), 'KATALON5567')
WebUI.setText(findTestObject('Page_SDMC/input_input-ConfirmPassword'), 'KATALON5567')
WebUI.click(findTestObject('Page_SDMC/button_Submit'))

//1 second delay for the login error label to change from "Changing password and signing in.."
WebUI.delay(1)

//Verify that the correct loging error text is displayed
WebUI.verifyElementText(findTestObject("Page_SDMC/LoginErrorText"), 'Wrong username or password.')

//Verify that the color of the error text is in red
//css_color = WebUI.getCSSValue(findTestObject('Page_SDMC/LoginErrorText'), 'color')
//WebUI.verifyEqual(css_color, 'rgba(239, 102, 102, 1)')
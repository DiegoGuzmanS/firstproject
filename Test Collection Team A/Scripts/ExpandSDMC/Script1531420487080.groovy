import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
//--------------------------------------------------------------------------------------------
import org.openqa.selenium.By as By

import org.openqa.selenium.WebDriver as WebDriver

import org.openqa.selenium.WebElement as WebElement

import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory

import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import  com.kms.katalon.core.testobject.ConditionType

WebUI.openBrowser('')
WebUI.navigateToUrl(GlobalVariable.SDMC_URL)
WebUI.setText(findTestObject('SDMC_Verify_Quad_Locations/input_input-username'), 'Diegoo')
WebUI.setText(findTestObject('SDMC_Verify_Quad_Locations/input_input-password'), 'KATALON5567')
WebUI.click(findTestObject('SDMC_Verify_Quad_Locations/button_Sign In'))
WebUI.maximizeWindow()
'Get chrome driver'
WebDriver driver = DriverFactory.getWebDriver()

'Make a new test object that points to the locations div'
TestObject locations = new TestObject()
locations.addProperty("id", ConditionType.EQUALS, "locations")

'Find the location div by its id '
WebUI.waitForElementPresent(locations, 10)
WebElement table = driver.findElement(By.id("locations"))

'Find the divs for Location A1, A2, A3..... B4 and store in a list'
List<WebElement> children = table.findElements(By.xpath("//*[contains(@id, 'location-')]"))

for(def i = 0; i < children.size(); i++)
{
	currentQuad = children.get(i) //currentQuad will be A1 or A2 or A3 or ...B4 depending on i
	quadName = currentQuad.getText() //To compare with hidden div later
	
	WebUI.callTestCase(findTestCase("ExpandQuadByTitle"), ["curQuad" : currentQuad], FailureHandling.CONTINUE_ON_FAILURE)
	WebUI.callTestCase(findTestCase("ExpandQuadBySpan"), ["curQuad" : currentQuad], FailureHandling.CONTINUE_ON_FAILURE)
	WebUI.callTestCase(findTestCase("ExpandQuadByArrow"), ["curQuad" : currentQuad], FailureHandling.CONTINUE_ON_FAILURE)
	
	'Expand current quad'
	currentQuad.click() //Expand quad
	
	def subLocationList = currentQuad.findElements(By.xpath("*")) //SubLocationList will contain the sublocations of the current quad
	
	for(def j = 0; j < subLocationList.size(); j++)
	{
		currentSubLocation = subLocationList.get(j)
		
		if(currentSubLocation.getText() != quadName) //Ignore the hidden div (the span of the quad)
		{
			def subLocation = currentSubLocation.findElements(By.xpath("*")) //Grab the immediate children nodes of the sublocation
			
			for(def k = 0; k < subLocation.size(); k++)
			{
				currentSub = subLocation.get(k)
				WebUI.callTestCase(findTestCase("ExpandSubByTitle"), ["curSub" : currentSub, "curQuad": quadName], FailureHandling.CONTINUE_ON_FAILURE)
				WebUI.callTestCase(findTestCase("ExpandSubBySpan"), ["curSub" : currentSub, "curQuad": quadName], FailureHandling.CONTINUE_ON_FAILURE)
				WebUI.callTestCase(findTestCase("ExpandSubByArrow"), ["curSub" : currentSub, "curQuad": quadName], FailureHandling.CONTINUE_ON_FAILURE)
				
				curSubName = currentSub.getText() // Store name to ignore hidden div later
				
				currentSub.click()
				
				def videoWallDivs = currentSub.findElements(By.xpath("*"))
				
				for(def l = 0; l < videoWallDivs.size(); l++)
				{
					curVidWall  = videoWallDivs.get(l)
					if(curVidWall.getText() != curSubName)
					{
						def videoWall = curVidWall.findElements(By.xpath("*")) //Isolate each video wall
						
						for(def h = 0; h < videoWall.size(); h++)
						{
							vidWall  = videoWall.get(h)
							WebUI.callTestCase(findTestCase("ExpandVidWallByTitle"), ["curVid" : vidWall, "curQuad": quadName, "curSub": curSubName], FailureHandling.CONTINUE_ON_FAILURE)
							WebUI.callTestCase(findTestCase("ExpandVidWallBySpan"), ["curVid" : vidWall, "curQuad": quadName, "curSub": curSubName], FailureHandling.CONTINUE_ON_FAILURE)
							WebUI.callTestCase(findTestCase("ExpandVidWallByArrow"), ["curVid" : vidWall, "curQuad": quadName, "curSub": curSubName], FailureHandling.CONTINUE_ON_FAILURE)
							vidWall.click()
						}
					}
				}
			}
		}
	}
	TestObject quad = new TestObject()
	quad.addProperty("tag", ConditionType.EQUALS, "span")
	quad.addProperty("text", ConditionType.EQUALS, quadName)
	WebUI.click(quad)
}

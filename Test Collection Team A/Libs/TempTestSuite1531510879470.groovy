import com.kms.katalon.core.logging.KeywordLogger
import com.kms.katalon.core.exception.StepFailedException
import com.kms.katalon.core.reporting.ReportUtil
import com.kms.katalon.core.main.TestCaseMain
import com.kms.katalon.core.testdata.TestDataColumn
import groovy.lang.MissingPropertyException
import com.kms.katalon.core.testcase.TestCaseBinding
import com.kms.katalon.core.driver.internal.DriverCleanerCollector
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.configuration.RunConfiguration
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import internal.GlobalVariable as GlobalVariable

Map<String, String> suiteProperties = new HashMap<String, String>();


suiteProperties.put('id', 'Test Suites/login')

suiteProperties.put('name', 'login')

suiteProperties.put('description', '')
 

DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.webui.contribution.WebUiDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.mobile.contribution.MobileDriverCleaner())



RunConfiguration.setExecutionSettingFile("C:\\Users\\geoff\\Desktop\\saamRepo\\test-collection-repo\\Test Collection Team A\\Reports\\login\\20180713_124119\\execution.properties")

TestCaseMain.beforeStart()

TestCaseMain.startTestSuite('Test Suites/login', suiteProperties, [new TestCaseBinding('Test Cases/firstCreate', 'Test Cases/firstCreate',  null), new TestCaseBinding('Test Cases/Negative Login to Testing SDMC', 'Test Cases/Negative Login to Testing SDMC',  null), new TestCaseBinding('Test Cases/Login to SDMC', 'Test Cases/Login to SDMC',  null), new TestCaseBinding('Test Cases/NewPasswordsDontMatch', 'Test Cases/NewPasswordsDontMatch',  null), new TestCaseBinding('Test Cases/PasswordLessThan6Chars', 'Test Cases/PasswordLessThan6Chars',  null), new TestCaseBinding('Test Cases/IncorrectCurrentPassword', 'Test Cases/IncorrectCurrentPassword',  null), new TestCaseBinding('Test Cases/ChangePasswordCORRECT', 'Test Cases/ChangePasswordCORRECT',  null)])

package internal

import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase

/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */
public class GlobalVariable {
     
    /**
     * <p></p>
     */
    public static Object EnvUrl
     
    /**
     * <p></p>
     */
    public static Object EnvUrl_R
     
    /**
     * <p></p>
     */
    public static Object SDMC_URL
     

    static {
        def allVariables = [:]        
        allVariables.put('default', [:])
        allVariables.put('DEV', allVariables['default'] + ['EnvUrl' : 'http://sds-dev-cloud.azurewebsites.net', 'EnvUrl_R' : '', 'SDMC_URL' : ''])
        allVariables.put('Intern', allVariables['default'] + ['EnvUrl' : 'http://sds-katalon.azurewebsites.net', 'EnvUrl_R' : '', 'SDMC_URL' : 'http://sdmc-katalon.azurewebsites.net/'])
        allVariables.put('QA_CLOUD', allVariables['default'] + ['EnvUrl' : 'http://sds-qa-cloud-1.azurewebsites.net/', 'EnvUrl_R' : 'http://sds-qa-cloud-2.azurewebsites.net/', 'SDMC_URL' : 'http://sdmc-qa-cloud.azurewebsites.net'])
        allVariables.put('QA_LAB', allVariables['default'] + ['EnvUrl' : 'http://192.168.0.220/synectDataServer/', 'SDMC_URL' : 'http://192.168.0.220/sdmc/', 'EnvUrl_R' : 'http://192.168.0.221/synectDataServer/'])
        
        String profileName = RunConfiguration.getExecutionProfile()
        
        def selectedVariables = allVariables[profileName]
        EnvUrl = selectedVariables['EnvUrl']
        EnvUrl_R = selectedVariables['EnvUrl_R']
        SDMC_URL = selectedVariables['SDMC_URL']
        
    }
}

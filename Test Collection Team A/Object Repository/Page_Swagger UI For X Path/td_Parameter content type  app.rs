<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>td_Parameter content type  app</name>
   <tag></tag>
   <elementGuidId>eb50d878-2633-4fff-8d3b-f468afe848a8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//td[(text() = '
				
				
				
				Parameter content type:

  application/json
  text/json
  application/xml
  text/xml
  application/x-www-form-urlencoded


' or . = '
				
				
				
				Parameter content type:

  application/json
  text/json
  application/xml
  text/xml
  application/x-www-form-urlencoded


')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
				
				
				
				Parameter content type:

  application/json
  text/json
  application/xml
  text/xml
  application/x-www-form-urlencoded


</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;SecurityModuleApi_SecurityModuleApi_RemoveUser_content&quot;)/form[@class=&quot;sandbox&quot;]/table[@class=&quot;fullwidth&quot;]/tbody[@class=&quot;operation-params&quot;]/tr[1]/td[2]</value>
   </webElementProperties>
</WebElementEntity>

<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Locations</name>
   <tag></tag>
   <elementGuidId>2a7db9ab-5fce-43cb-b5f2-5db8e48ece79</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>locations</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Location: A1Sub Location: Check-inVideo Wall 1Counter IDAirlineStation IdentifierForced On1991 - 1886JetblueExpand to ViewN/AVideo Wall 2Counter IDAirlineStation IdentifierForced On1877 - 1823JetblueExpand to ViewN/A1808 - 1805GOAA ExperienceN/AN/A1796 - 1751AeromexicoExpand to ViewN/AVideo Wall 3Counter IDAirlineStation IdentifierForced On1744 - 1704AzulExpand to ViewYesSub Location: SELF1Video Wall 1Counter IDAirlineStation IdentifierForced On17001 - 17002JetblueExpand to ViewN/ASub Location: SELF2Video Wall 1Counter IDAirlineStation IdentifierForced On17003 - 17004AzulExpand to ViewYesSub Location: CURB1Video Wall 1Counter IDAirlineStation IdentifierForced On17011 - 17012GOAA ExperienceN/AN/ASub Location: CURB2Video Wall 1Counter IDAirlineStation IdentifierForced On17013 - 17014GOAA ExperienceN/AN/ALocation: A2Sub Location: Check-inVideo Wall 1Counter IDAirlineStation IdentifierForced On2786Copa AirlinesInterJetAlaskaAviancaMiami AirCopa AirlinesNorwegianEurowingsFrontierGOAA ExperienceSunwingWorld AtlanticXtra AirwaysDo Not ShowN/AVideo Wall 2Counter IDAirlineStation IdentifierForced On2782 - 2770Copa AirlinesExpand to ViewN/A2764 - 2762GOAA ExperienceN/AN/A2756 - 2741AviancaExpand to ViewNoVideo Wall 3Counter IDAirlineStation IdentifierForced On2731GOAA ExperienceInterJetAlaskaAviancaMiami AirCopa AirlinesNorwegianEurowingsFrontierGOAA ExperienceSunwingWorld AtlanticXtra AirwaysN/AN/A2726 - 2672AlaskaExpand to ViewNoVideo Wall 4Counter IDAirlineStation IdentifierForced On2663 - 2618GOAA ExperienceN/AN/A2615 - 2528FrontierExpand to ViewN/ASub Location: SELF1Video Wall 1Counter IDAirlineStation IdentifierForced On25001 - 25002GOAA ExperienceN/AN/ASub Location: SELF2Video Wall 1Counter IDAirlineStation IdentifierForced On25003 - 25004GOAA ExperienceN/AN/ASub Location: CURB1Video Wall 1Counter IDAirlineStation IdentifierForced On25011 - 25012GOAA ExperienceN/AN/ASub Location: CURB2Video Wall 1Counter IDAirlineStation IdentifierForced On25013 - 25014GOAA ExperienceN/AN/ALocation: A3Sub Location: Check-inVideo Wall 1Counter IDAirlineStation IdentifierForced On2992 - 2920LATAMExpand to ViewNo2914GOAA ExperienceAer LingusEmiratesGOAA ExperienceSunwingLATAMAir TransatVirgin AtlanticN/AN/A2905 - 2887Air TransatExpand to ViewNo2878 - 2872GOAA ExperienceN/AN/AVideo Wall 2Counter IDAirlineStation IdentifierForced On2866 - 2862GOAA ExperienceN/AN/A2850 - 2802Virgin AtlanticExpand to ViewN/ASub Location: SELF1Video Wall 1Counter IDAirlineStation IdentifierForced On28001 - 28002GOAA ExperienceN/AN/ASub Location: SELF2Video Wall 1Counter IDAirlineStation IdentifierForced On28003 - 28004Virgin AtlanticExpand to ViewN/ASub Location: CURB1Video Wall 1Counter IDAirlineStation IdentifierForced On28011 - 28012GOAA ExperienceN/AN/ASub Location: CURB2Video Wall 1Counter IDAirlineStation IdentifierForced On28013 - 28014GOAA ExperienceN/AN/ALocation: A4Sub Location: Check-inVideo Wall 1Counter IDAirlineStation IdentifierForced On3741 - 3594SouthwestN/AN/AVideo Wall 2Counter IDAirlineStation IdentifierForced On3588 - 3502SouthwestN/AN/ASub Location: SELF1Video Wall 1Counter IDAirlineStation IdentifierForced On35001 - 35002GOAA ExperienceN/AN/ASub Location: SELF2Video Wall 1Counter IDAirlineStation IdentifierForced On35003 - 35004GOAA ExperienceN/AN/ASub Location: CURB1Video Wall 1Counter IDAirlineStation IdentifierForced On35011 - 35012SouthwestN/AN/ASub Location: CURB2Video Wall 1Counter IDAirlineStation IdentifierForced On35013 - 35014GOAA ExperienceN/AN/ALocation: B1Sub Location: Check-inVideo Wall 1Counter IDAirlineStation IdentifierForced On7202 - 7293SpiritExpand to ViewN/A7305 - 7329GOAA ExperienceN/AN/A7332 - 7350Silver AirwaysExpand to ViewN/AVideo Wall 2Counter IDAirlineStation IdentifierForced On7362 - 7518UnitedExpand to ViewN/ASub Location: SELF1Video Wall 1Counter IDAirlineStation IdentifierForced On72001 - 72002GOAA ExperienceN/AN/ASub Location: SELF2Video Wall 1Counter IDAirlineStation IdentifierForced On72003 - 72004GOAA ExperienceN/AN/ASub Location: CURB1Video Wall 1Counter IDAirlineStation IdentifierForced On72011 - 72012GOAA ExperienceN/AN/ASub Location: CURB2Video Wall 1Counter IDAirlineStation IdentifierForced On72013 - 72014GOAA ExperienceN/AN/ALocation: B2Sub Location: Check-inVideo Wall 1Counter IDAirlineStation IdentifierForced On8104 - 8177AmericanExpand to ViewN/AVideo Wall 2Counter IDAirlineStation IdentifierForced On8195 - 8281AmericanExpand to ViewN/AVideo Wall 3Counter IDAirlineStation IdentifierForced On8289 - 8385British AirwaysExpand to ViewN/ASub Location: SELF1Video Wall 1Counter IDAirlineStation IdentifierForced On80001 - 80002GOAA ExperienceN/AN/ASub Location: SELF2Video Wall 1Counter IDAirlineStation IdentifierForced On80003 - 80004GOAA ExperienceN/AN/ASub Location: CURB1Video Wall 1Counter IDAirlineStation IdentifierForced On80011 - 80012AmericanExpand to ViewN/ASub Location: CURB2Video Wall 1Counter IDAirlineStation IdentifierForced On80013 - 80014GOAA ExperienceN/AN/ALocation: B3Sub Location: Check-inVideo Wall 1Counter IDAirlineStation IdentifierForced On8406 - 8445Air CanadaExpand to ViewN/A8447 - 8463GOAA ExperienceN/AN/A8465 - 8495Thomas CookExpand to ViewYesVideo Wall 2Counter IDAirlineStation IdentifierForced On8501 - 8517IcelandairExpand to ViewNo8523 - 8539GOAA ExperienceN/AN/A8541 - 8555WestjetExpand to ViewN/ASub Location: SELF1Video Wall 1Counter IDAirlineStation IdentifierForced On84001 - 84002GOAA ExperienceN/AN/ASub Location: SELF2Video Wall 1Counter IDAirlineStation IdentifierForced On84003 - 84004GOAA ExperienceN/AN/ASub Location: CURB1Video Wall 1Counter IDAirlineStation IdentifierForced On84011 - 84012GOAA ExperienceN/AN/ASub Location: CURB2Video Wall 1Counter IDAirlineStation IdentifierForced On84013 - 84014GOAA ExperienceN/AN/ALocation: B4Sub Location: Check-inVideo Wall 1Counter IDAirlineStation IdentifierForced On9207 - 9299DeltaN/AN/AVideo Wall 2Counter IDAirlineStation IdentifierForced On9303 - 9317GOAA ExperienceN/AN/A9319 - 9335CaribbeanExpand to ViewNo9341GOAA ExperienceBahamasairCaribbeanDeltaLufthansaEdelweiss AirGOAA ExperienceSun CountryN/AN/A9343 - 9357Sun CountryN/ANo9359 - 9378BahamasairExpand to ViewN/ASub Location: SELF1Video Wall 1Counter IDAirlineStation IdentifierForced On92001 - 92002DeltaN/AN/ASub Location: SELF2Video Wall 1Counter IDAirlineStation IdentifierForced On92003 - 92004GOAA ExperienceN/AN/ASub Location: CURB1Video Wall 1Counter IDAirlineStation IdentifierForced On92011 - 92012DeltaN/AN/ASub Location: CURB2Video Wall 1Counter IDAirlineStation IdentifierForced On92013 - 92014GOAA ExperienceN/AN/A</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;locations&quot;)</value>
   </webElementProperties>
</WebElementEntity>
